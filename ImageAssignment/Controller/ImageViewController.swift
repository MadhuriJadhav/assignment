//
//  ImageViewController.swift
//  ImageAssignment
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import UIKit
import SDWebImage

class ImageViewController: UIViewController {

    let viewModel = ImageDataViewModel(dataService: DataService())
    private var imageListTableView: UITableView!
    private let refreshControl = UIRefreshControl()

    /// Description:- It loads the View initially before the view is going to appear on the window.
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTableView()
        self.getImageDataFromAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        imageListTableView.layoutIfNeeded()
    }
    
    ///Description - sets up TableView initially and is added on the controller
    func setUpTableView() {
        imageListTableView = UITableView(frame: UIScreen.main.bounds)
        imageListTableView.register(ImageViewCell.self, forCellReuseIdentifier: "cell")
        imageListTableView.dataSource = self
        imageListTableView.delegate = self
        imageListTableView.translatesAutoresizingMaskIntoConstraints = false
        imageListTableView.separatorStyle = .none
        imageListTableView.estimatedRowHeight = 200
        imageListTableView.rowHeight = UITableView.automaticDimension
        
        self.view.addSubview(imageListTableView)
        imageListTableView.leftAnchor.constraint(
            equalTo: view.leftAnchor,
            constant: 0).isActive = true
        imageListTableView.topAnchor.constraint(
            equalTo: view.topAnchor,
            constant: 0).isActive = true
        imageListTableView.rightAnchor.constraint(
            equalTo: view.rightAnchor,
            constant: 0).isActive = true
        imageListTableView.bottomAnchor.constraint(
            equalTo: view.bottomAnchor,
            constant: 0).isActive = true
        
        addRefreshControl()
    }
    
    ///Description - This function adds the refresh controller on tableview and configures it.
    func addRefreshControl() {
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            imageListTableView.refreshControl = refreshControl
        } else {
            imageListTableView.addSubview(refreshControl)
        }
        // Configure Refresh Control
        refreshControl.addTarget(self, action: #selector(refreshImgeData(_:)), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Fetching Image Data ...")
    }
    
    ///Description - this method is called when tableview is refreshed via refresh control
    ///and fetches the data from API again
    @objc private func refreshImgeData(_ sender: Any) {
        getImageDataFromAPI()
        self.refreshControl.endRefreshing()
    }
    
    ///Description - This method calls the method from view model which gives the API call.
    ///Handles the work after successfull data fetch as well as the after the error is returned.
    fileprivate func getImageDataFromAPI() {
        self.view.showSpinner()
        viewModel.fetchImageDataFromAPI()
        
        // After successfull data fetch, reload the table
        viewModel.didFinishFetch = {
            //set the title to navigation bar
            self.navigationItem.title = self.viewModel.navigationBarTitle
            DispatchQueue.main.async {
                self.view.hideSpinner()
                //reload the table
                self.imageListTableView.reloadData()
            }
        }
        
        // when the error is returned after API call, show alert on controller stating
        // the error occured
        viewModel.didFailWithError = {
            self.view.hideSpinner()
            if let errorMessage = self.viewModel.errorMessage {
                self.refreshControl.endRefreshing()
                self.alert(message: errorMessage)
            }
        }
    }
}

extension UIViewController {
    // MARK: - Extension to show Alert on View Controller
    ///Description - UIAlertController is initialised and configured to show error that has
    ///been returned from API.
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
