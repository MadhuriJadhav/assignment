//
//  ImageVC-Extension+TableViewDelegate.swift
//  ImageAssignment
//
//  Created by Madhuri on 11/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import UIKit

extension ImageViewController: UITableViewDelegate {
// MARK: - Table View Delegate Methods
    private func tableView(tableView: UITableView,
                           heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    private func tableView(tableView: UITableView,
                           estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
