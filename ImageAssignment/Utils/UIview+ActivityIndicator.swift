//
//  ViewControllerUtils.swift
//  ImageAssignment
//
//  Created by Madhuri on 11/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import UIKit

extension UIView {
    // MARK: - Extension to show Spinner on View
    
    // Description - This sets up the spinner initially and adds it on the view
    func showSpinner() {
        //small view with black background is initialised and added at the center of view
        let backgroundView = UIView()
        backgroundView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        backgroundView.backgroundColor = UIColor(white: 0, alpha: 0.7)
        backgroundView.tag = 475647
        backgroundView.center = self.center
        backgroundView.layer.cornerRadius = 10

        // Activity Indicator is initialised and added on the black background view
        let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
        activityIndicator.center = CGPoint(x: backgroundView.frame.size.width / 2, y:
               backgroundView.frame.size.height / 2)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .white
        activityIndicator.startAnimating()
        
        backgroundView.addSubview(activityIndicator)
        self.addSubview(backgroundView)
    }

    //Description - This function removes the spinner from view
    func hideSpinner() {
        if let background = viewWithTag(475647) {
            background.removeFromSuperview()
        }
    }
}
