//
//  DataService.swift
//  ImageAssignment
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - Protocol
protocol DataServiceProtocol {
    ///Description - This function gives the API call and successfull response is mapped
    ///to the ImageDataModel and If error is returned then return the error in completion handler.
    func requestforImageInfo(completion: @escaping (ImageDataModel?, String?) -> Void)
}

struct DataService: DataServiceProtocol {
    // MARK: - Singleton
    static let shared = DataService()
    // MARK: - URLs
    private var dataUrl =  "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    // MARK: - APIService call to fetch data
    
    ///Description - This function gives the API call and successfull response is mapped
    ///to the ImageDataModel and If error is returned then return the error in completion handler.
    /// - Parameter completion - completion: @escaping (ImageDataModel?, String?) -> Void :-
    /// This completion block returns the ImageDataModel and the error occured if any
    func requestforImageInfo(completion: @escaping (ImageDataModel?, String?) -> Void) {
        Alamofire.request(self.dataUrl).validate().responseString { (response) in
            if response.result.isSuccess {
                guard let data = response.value?.data(using: .utf8) else { return }
                do {
                    // Map response data to model here
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(ImageDataModel.self, from: data)
                    completion(model, nil)
                } catch let error {
                    print(error.localizedDescription)
                    completion(nil, error.localizedDescription)
                }
            } else {
                completion(nil, response.result.error?.localizedDescription)
            }
        }
    }
}
