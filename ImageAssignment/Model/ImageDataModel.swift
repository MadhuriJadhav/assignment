//
//  ImageData.swift
//  ImageAssignment
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import Foundation
import Alamofire

/// Description - ImageDataModel to map service response accordingly to particular values
/// with keys title and rows
struct ImageDataModel: Codable {
    var title: String
    var imageInfo: [ImageInfo]
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case imageInfo = "rows"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title) ?? "No Title"
        imageInfo = try values.decodeIfPresent([ImageInfo].self, forKey: .imageInfo) ?? []
    }
}

/// Description - ImageInfo to map service response accordingly, to particular values
/// with values from key "rows" of the response
struct ImageInfo: Codable {
    var title, description, imageHref: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case description
        case imageHref
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title) ?? ""
        description = try values.decodeIfPresent(String.self, forKey: .description) ?? ""
        imageHref = try values.decodeIfPresent(String.self, forKey: .imageHref) ?? ""
    }
}
