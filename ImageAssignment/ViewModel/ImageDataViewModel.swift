//
//  ImageDataViewModel.swift
//  ImageAssignment
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import Foundation

class ImageDataViewModel {
    // MARK: - Private
    private var dataService: DataService?
    
    //ImageList variable is set when image data from API is successfully fetched
    private var imageList: ImageDataModel? {
        didSet {
            guard let imageModel = imageList else { return }
            setDataToProperties(model: imageModel)
            self.didFinishFetch?()
        }
    }
    
    // MARK: - Public
    var navigationBarTitle: String?
    var imageDataInfo = [ImageInfo]()
    var errorMessage: String?
        
    // MARK: - Closures
    ///Description - Closure will be called when data fetched successfully from API
    var didFinishFetch: (() -> Void)?
    ///Description - Closure will be called when API returns the error
    var didFailWithError: (() -> Void)?
    
    // MARK: - Method calls
    /// Description - This function sets the values received fron successfull API response like
    /// navigation bar title and values to bbe shown on table view cell (rows) to the propertites
    /// in view model
    /// - Parameter model - It is the ImageDataModel to set the values to properties of navigation
    /// bar title and tableview cells
    func setDataToProperties(model: ImageDataModel) {
        self.navigationBarTitle = model.title // used for setting the navigation bar title
        self.imageDataInfo = model.imageInfo // used for values to show on tableview cells
    }
    
    // MARK: - Network Call
    /// Description - This function calls the service method for fetching data from API
    func fetchImageDataFromAPI() {
        //After the API hit, parsed ImageList (i.e ImageDataModel) from response and error if any,
        //is returned after completion
        self.dataService?.requestforImageInfo(completion: { (imageList, error) in
            if let error = error {
                //If error has occured then didFailWithError method is called to show the error
                //on controller
                self.errorMessage = error
                self.didFailWithError?()
                return
            }
            self.imageList = imageList
        })
    }
    
    // MARK: - Constructor
    init(dataService: DataService) {
        self.dataService = dataService
    }
}
