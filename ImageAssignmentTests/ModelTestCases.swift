//
//  ModelTestCases.swift
//  ImageAssignmentTests
//
//  Created by Madhuri on 18/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import XCTest
@testable import ImageAssignment

class ModelTestCases: XCTestCase {
    private var imageDataModel: ImageDataModel?
    private var imageInfoArray = [ImageInfo]()
    private var imageInfo: ImageInfo?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
       let jsonObject: [String: Any] = [
            "title": "Country Name",
            "rows": [
                ["title": "Feature 1",
                "description": "Description of Feature 1",
                "imageHref": "https://"]
            ]
        ]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: jsonObject)
            let decoder = JSONDecoder()
            let model = try decoder.decode(ImageDataModel.self, from: jsonData)
            imageDataModel = model
            imageInfoArray = imageDataModel!.imageInfo
            imageInfo = imageInfoArray[0]
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func testIfModelHasData() {
        XCTAssertEqual(imageInfo?.title , "Feature 1", "title Proprty should not change post Initialization")
        XCTAssertEqual(imageInfo?.description, "Description of Feature 1", "description Proprty should not change post Initialization")
        XCTAssertEqual(imageInfo?.imageHref, "https://", "imageHref Proprty should not change post Initialization")
    }
    
    func testIfTitleHasValue() {
        XCTAssertEqual(imageDataModel?.title , "Country Name", "title Proprty should not change post Initialization")
    }
    
    func testIfRowsHaveVlue() {
        XCTAssertNotNil(imageDataModel?.imageInfo, "rows Proprty should not change post Initialization")
    }
}

