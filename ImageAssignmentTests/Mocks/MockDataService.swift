//
//  MockDataService.swift
//  ImageAssignmentTests
//
//  Created by Madhuri on 18/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import Foundation
@testable import ImageAssignment

class MockDataService {
    // Mock Data service class is created to generate mock response
    var shouldReturnError = false
    var wasrequestforImageInfoCalled = false
    var invalidJson = false
    private var imageDataModel: ImageDataModel?
    
    enum MockServiceError: Error {
        case requestforImageInfoError //Error Returned from API
        case invalidJsonError //Invalid JSON error
    }
    
    func reset() {
        shouldReturnError = false
        wasrequestforImageInfoCalled = false
    }
    
    init() {
        self.createMockData()
    }
    
    // This creates the mock data model
    func createMockData() {
        let mockJsonObject: [String: Any] = [
            "title": "Country Name",
            "rows": [
                ["title": "Feature 1",
                "description": "Description of Feature 1",
                "imageHref": "https://"]
            ]
        ]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: mockJsonObject)
            let decoder = JSONDecoder()
            let model = try decoder.decode(ImageDataModel.self, from: jsonData)
            self.imageDataModel = model
        } catch {
            invalidJson = true
        }
    }
}

// this method will return the mock responses as per requirement
extension MockDataService: DataServiceProtocol {
    func requestforImageInfo(completion: @escaping (ImageDataModel?, String?) -> Void) {
        wasrequestforImageInfoCalled = true
        if invalidJson {
            completion(nil, MockServiceError.invalidJsonError.localizedDescription)
        } else if shouldReturnError {
            completion(nil, MockServiceError.requestforImageInfoError.localizedDescription)
        } else {
            completion(self.imageDataModel, nil)
        }
    }
}
