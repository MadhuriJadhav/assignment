//
//  ImageAssignmentTests.swift
//  ImageAssignmentTests
//
//  Created by Madhuri on 08/02/20.
//  Copyright © 2020 Madhuri. All rights reserved.
//

import XCTest
@testable import ImageAssignment

class DataServiceTestCases: XCTestCase {
    
    private let mockDataService = MockDataService()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    //Test for API failure
    func testErrorFromAPI() {
        let expectation = self.expectation(description: "API Returned error")
        mockDataService.shouldReturnError = true
        mockDataService.requestforImageInfo { (imageDataModel, error) in
            XCTAssertNil(imageDataModel)
            guard let error = error else {
                XCTFail()
                return
            }
            XCTAssertNotNil(error)
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    //Test for Successfull API hit
    func testSuccessfullImageDataFromAPI() {
        let expectation = self.expectation(description: "Successfull API hit")
        mockDataService.shouldReturnError = false
        mockDataService.requestforImageInfo { (imageDataModel, error) in
            XCTAssertNil(error)
            guard let imageDataModel = imageDataModel else {
                XCTFail()
                return
            }
            XCTAssertNotNil(imageDataModel)
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 10.0, handler: nil)
    }
    
    //Test for whether the request call was made
    func testRequestforImageInfoCalled() {
        mockDataService.requestforImageInfo { (imageDataModel, error) in
            if self.mockDataService.wasrequestforImageInfoCalled {
                XCTAssertEqual(self.mockDataService.wasrequestforImageInfoCalled, true)
            } else {
                XCTFail()
            }
        }
    }
    
    //Test for Invalid JSON
    func testIfInvalidJSON() {
        mockDataService.requestforImageInfo { (imageDataModel, error) in
            if self.mockDataService.invalidJson {
                XCTAssertEqual(self.mockDataService.invalidJson, true)
            } else {
                XCTAssertFalse(self.mockDataService.invalidJson, "JSON is valid")
            }
        }
    }

    func testCheckAPI_Response() {
        let viewModel = ImageDataViewModel(dataService: DataService())
        viewModel.fetchImageDataFromAPI()
        viewModel.didFinishFetch = {
            XCTAssert(true)
        }
        viewModel.didFailWithError = {
            XCTAssert(false)
        }
    }

    override func tearDown() {
            // Put teardown code here. This method is called after the invocation of each test method in the class.
        mockDataService.reset()
    }
        
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
